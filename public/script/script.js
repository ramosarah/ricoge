// Connexion à socket.io
var socket = io.connect('/'); // / ou http://localhost:8080


//taleau asosciatif avec clé= speudo et valeur= couleur à chaque reconnection véfier que le speudo existe ou pas.


// Ajoute un message dans la page
function insereMessage(pseudo, message) {
    const color = "#" + Math.floor(Math.random()*16777215).toString(16); // TODO: rendre aléatoire
    $('#zone_chat').append('<div><em id="pseudo" style="color:'+ color +';">' + pseudo + '</em> ' + message + '</div>');
}


// On demande le pseudo, on l'envoie au serveur et on l'affiche dans le titre
var pseudo = prompt('Pseudo?');

socket.emit('nouveau_client', pseudo);
document.title = pseudo + ' - ' + document.title;

// Quand un nouveau client se connecte, on affiche l'information
socket.on('nouveau_client', function(pseudo) {
    $('#zone_chat').append('<p><em>' + pseudo + ' </em> a rejoint le Chat !</p>');
})

// Quand on reçoit un message, on l'insère dans la page
socket.on('message', function(data) {
    insereMessage(data.pseudo, data.message)
})


socket.on('compteur', function(compteur) {
  $('#compteur').append('<p><em>' + compteur + '</em></p>');
})

socket.on('randomColor', function(pseudo, randomColor) {
})

// Lorsqu'on envoie le formulaire, on transmet le message et on l'affiche sur la page
$('#formulaire_chat').submit(function () {
    var message = $('#message').val();
    socket.emit('message', message); // Transmet le message aux autres
    insereMessage(pseudo, message); // Affiche le message aussi sur notre page
    $('#message').val('').focus(); // Vide la zone de Chat et remet le focus dessus
    return false; // Permet de bloquer l'envoi "classique" du formulaire
});
