var express = require('express');
var app = express();
var http = require('http');
var fs = require('fs');
var server = http.createServer(app);
var io = require('socket.io').listen(server);
var ent = require('ent');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
//var randomColor = Math.floor(Math.random()*16777215).toString(16);


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
global.counter = 0;


// Chargement de la page index.html
app.get('/', function (req, res) {
  res.sendfile(__dirname + '/views/index.html');
});

app.get('/style.css', function (req, res) {
  res.sendfile(__dirname + '/views/index.html');
});

io.sockets.on('connection', function (socket, pseudo) {
    // Dès qu'on nous donne un pseudo, on le stocke en variable de session et on informe les autres personnes
    socket.on('nouveau_client', function(pseudo) {
        pseudo = ent.encode(pseudo);
        socket.pseudo = pseudo;
        socket.broadcast.emit('nouveau_client', pseudo);

        counter++;
        socket.compteur = counter;
        socket.broadcast.emit('compteur', counter);
    });



    // Dès qu'on reçoit un message, on récupère le pseudo de son auteur et on le transmet aux autres personnes
    socket.on('message', function (message) {
        message = ent.encode(message);
        socket.broadcast.emit('message', {pseudo: socket.pseudo, message: message});
    });


    socket.on('disconnect', function(pseudo) {
      pseudo = ent.encode(pseudo);
      socket.pseudo = pseudo;
      socket.broadcast.emit('depart_client', pseudo);

      counter--;
      socket.compteur = counter;
      socket.broadcast.emit('compteur', counter);
    })
});

server.listen(8080);
